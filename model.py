from database import Base
from sqlalchemy import Column, String, Integer, DateTime, Enum
import datetime
from schema import Roles

class User(Base):
    __tablename__ = 'users'
    id= Column(Integer, primary_key= True )
    email= Column(String(20), unique=True, index=True)
    hashed_password= Column(String(100))
    create_date= Column(DateTime, default= datetime.datetime.utcnow )
    role = Column(Enum(Roles))