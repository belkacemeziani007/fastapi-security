from sqlalchemy.orm import Session
from typing import Optional
from datetime import datetime, timedelta
from fastapi import HTTPException, status
from model import User
from schema import UserCreate, TokenData
from passlib.context import CryptContext
from jose import JWTError, jwt
import crud
import schema

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"

def create_user(db: Session, user: UserCreate):
    # hashed_password= bcrypt.hashpw(user.password.encode('utf-8'), bcrypt.gensalt())
    db_user= User(email=user.email, hashed_password=get_password_hash(user.password), role= schema.Roles.guest )
    db.add(db_user)
    db.commit()
    return db_user

def get_user(db: Session, email: str):
    return db.query(User).filter(User.email==email).first()

def delete_the_user(db:Session, user:User):
    db.delete(user)
    db.commit()


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

def decode_access_token(db, token):
   credentials_exception = HTTPException(
   	status_code=status.HTTP_401_UNAUTHORIZED,
   	detail="Could not validate credentials",
   	headers={"WWW-Authenticate": "Bearer"},
   )
   try:
   	payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
   	email: str = payload.get("sub")
   	if email is None:
           raise credentials_exception
   	token_data = TokenData(email=email)
   except JWTError:
   	raise credentials_exception
   user = crud.get_user(db, email=token_data.email)
   if user is None:
   	raise credentials_exception
   return user

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)