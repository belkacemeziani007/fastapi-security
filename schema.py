from pydantic import BaseModel, Field, EmailStr
from datetime import datetime
from typing import Optional
from enum import Enum

class Roles(str, Enum):
    guest = 'guest'
    member= 'member'
    admin= 'admin'

class UserBase(BaseModel):
    email: EmailStr
    

class UserCreate(UserBase):
    password: str = Field(...)

class User(UserBase):
    id : int = Field(...)
    create_date: datetime 
    role= Roles

    

    class Config:
      orm_mode = True

class UserInDB(User):
    hashed_password: str

class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None

    